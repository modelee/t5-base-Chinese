A mt5-base model that the vocab and word embedding are truncated, only Chinese and English characters are retained.
https://github.com/lemon234071/TransformerBaselines